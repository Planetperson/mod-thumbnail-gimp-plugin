# MOD Thumbnail GIMP Plugin

## Installing

In GIMP, go to Edit > Preferences > Folders > Plug-ins. This window will list
all of the folders on your computer that GIMP loads plugins from. All you need
to do is copy the file `mod_thumbnail_plugin.py` to one of the folders listed
here (use the first one).

After copying the file and restarting GIMP, the plugin will be available via
Image > Selection to Thumbnail.
