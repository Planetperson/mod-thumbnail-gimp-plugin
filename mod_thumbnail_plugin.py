#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

from gimpfu import *

def selection_to_thumbnail(image, drawable, padding_size, width, height):
    if not (0 <= padding_size <= 1):
        raise ValueError('padding_size ({}) must be between 0 and 1'.format(padding_size))
    if not (width >= 1):
        raise ValueError('width ({}) must be at least 1'.format(width))
    if not (height >= 1):
        raise ValueError('height ({}) must be at least 1'.format(height))
    crop_around_selection(image, padding_size, width, height)
    pdb.gimp_image_scale(image, width, height)

def satisfy_boundaries(image_size, sel_center, sel_size, final_size, padding_size):
    image_w, image_h = image_size
    width, height = final_size
    sel_center_x, sel_center_y = sel_center
    sel_w, sel_h = sel_size
    # Figure out padded size for width
    padded_half_w = sel_w / (2 * (1 - padding_size))
    padded_half_h = (height / width) * padded_half_w
    # Don't go past image edges, keep centered, rescale as necessary
    padded_x1 = sel_center_x - padded_half_w
    padded_x2 = sel_center_x + padded_half_w
    padded_y1 = sel_center_y - padded_half_h
    padded_y2 = sel_center_y + padded_half_h
    # Figure out the half-widths after clamping in all 4 directions
    clamped_left = sel_center_x - max(padded_x1, 0)
    clamped_right = min(padded_x2, image_w) - sel_center_x
    clamped_top = sel_center_y - max(padded_y1, 0)
    clamped_bottom = min(padded_y2, image_h) - sel_center_y
    # Figure out the scale of the smallest clamp -- this will satisfy all
    # boundaries
    scale = min(
        clamped_left / padded_half_w,
        clamped_right / padded_half_w,
        clamped_top / padded_half_h,
        clamped_bottom / padded_half_h
    )
    scaled_half_w = scale * padded_half_w
    scaled_half_h = scale * padded_half_h
    scaled_size = (2 * scaled_half_w, 2 * scaled_half_h)
    # Return how well the resized image satisfies the padding constraint (sum
    # of padding, clamped by padding size; higher is better)
    padding_w = min((scaled_half_w - sel_w / 2) / scaled_half_w, padding_size)
    padding_h = min((scaled_half_h - sel_h / 2) / scaled_half_h, padding_size)
    padding = padding_w + padding_h
    return scaled_size, padding

def crop_around_selection(image, padding_size, width, height):
    sel_exists, sel_x1, sel_y1, sel_x2, sel_y2 = pdb.gimp_selection_bounds(image)
    if not sel_exists:
        return
    pdb.gimp_selection_none(image)
    image_w = pdb.gimp_image_width(image)
    image_h = pdb.gimp_image_height(image)
    sel_w = sel_x2 - sel_x1
    sel_h = sel_y2 - sel_y1
    sel_half_w = sel_w / 2
    sel_half_h = sel_h / 2
    sel_center_x = sel_x1 + sel_half_w
    sel_center_y = sel_y1 + sel_half_h
    # Try satisfying the padding constraint using two cases:
    # (1) pad using width
    # (2) pad using height
    width_size, width_padding = satisfy_boundaries(
        image_size=(image_w, image_h),
        sel_center=(sel_center_x, sel_center_y),
        sel_size=(sel_w, sel_h),
        final_size=(width, height),
        padding_size=padding_size
    )
    height_size, height_padding = satisfy_boundaries(
        image_size=(image_h, image_w),
        sel_center=(sel_center_y, sel_center_x),
        sel_size=(sel_h, sel_w),
        final_size=(height, width),
        padding_size=padding_size
    )
    height_size = height_size[::-1]
    # Choose the size that better satisfies the padding constraints.
    if width_padding >= height_padding:
        new_size = width_size
    else:
        new_size = height_size
    # Resize the image
    new_w, new_h = new_size
    new_x1 = sel_center_x - new_w / 2
    new_y1 = sel_center_y - new_h / 2
    pdb.gimp_image_resize(image, new_w, new_h, -new_x1, -new_y1)

register(
    'selection_to_thumbnail',
    'Automatically convert the selected part of an image to a thumbnail',
    'Automatically convert the selected part of an image to a thumbnail',
    'Planetperson',
    'Planetperson',
    '2021',
    '<Image>/Image/Selection to Thumbnail',
    'RGB*, GRAY*',
    [
        (PF_FLOAT, 'padding_size', 'The amount of padding left around the selection, as a ratio between 0 and 1 of the size of the final image.', 0.04),
        (PF_INT, 'width', 'Final width of the image.', 1536),
        (PF_INT, 'height', 'Final height of the image.', 768)
    ],
    [],
    selection_to_thumbnail
)

main()
